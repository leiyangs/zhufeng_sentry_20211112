# 导航操作检测

## 基本操作检测

- 跳转路由 中绑定点击事件

```javascript

fn() {
  console.log(window.a.b);
}

```

- 在 Sentry 管理界面 -> Discover 菜单 -> All Events 中，我们可以看到刚才上报的 transaction: /about，点击打开详情

- 在 Breadcrumbs 中，我们可以看到这是一个 TYPE 为 navigation，CATEGORY 为 navigation 的 transaction，并告诉了我们 from，从 / 路由，to，跳转到了 /about 路由。

- 在跳转之前，经历了 pageload，和 ui.click

## 导航操作检测

- 对于 /user/:id 的路由，不同的 id，上报的 transaction 名称是不同的，例如 /user/1、/user/2。

- 我们可以分别在 /user/1、/user/2 路由下刷新页面，然后在 Sentry 管理界面的 Performance 菜单里查看效果，会看到两条不同的 TRANSACTION: /user/1、/user/2

- 安装 history `npm i -S history@4`
